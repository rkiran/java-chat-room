import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Client
{
	public static void main(String args[]) throws Exception
	{
		Socket s = new Socket("127.0.0.1",9990);
		BufferedReader sin = new BufferedReader(new InputStreamReader(s.getInputStream())); 
		PrintStream sout = new PrintStream(s.getOutputStream());//The PrintStream class can format primitive types like int , long etc. formatted as text, rather than as their byte values
		Scanner t = new Scanner(System.in);
		String str;
		while(true) //used for continuous chatting unless client says bye
		{
			System.out.print("Client: ");
			str = t.nextLine();
			sout.println(str);
			if(str.equalsIgnoreCase("bye"))
			{
				System.out.println("Connection ended by client");
				break;
			}
			str = sin.readLine();
			System.out.println("Server: "+str+"\n");
			
		}
		s.close();
		t.close();
	}
}
