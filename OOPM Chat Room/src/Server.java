import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server 
{
	public static void main(String[] args) throws Exception
	{
		ServerSocket ss = new ServerSocket(9990);
		Socket s = ss.accept(); //creates a socket only after server socket accepts data
		BufferedReader cin = new BufferedReader(new InputStreamReader(s.getInputStream()));//gets client socket input
		PrintStream  cout  = new PrintStream(s.getOutputStream());
		String str;
		Scanner t = new Scanner(System.in);
		while(true)
		{
			str = cin.readLine();//gets client socket input
			System.out.print("Client: "+str+"\n");
			System.out.print("Server: ");
			str = t.nextLine();
			if(str.equalsIgnoreCase("BYE"))
			{
				cout.println("BYE");
				System.out.print("Connection ended by Server");
				break;
			}
			cout.println(str);//will print the output by the server side if the client does type "bye" and break
		}
		ss.close();
		s.close();
		t.close();
	}

}
